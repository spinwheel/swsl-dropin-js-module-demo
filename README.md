[![test image size](https://cdn.spinwheel.io/images/logo/spinwheel-logo-dark.png)](https://www.spinwheel.io/)
# SpinWheel Drop in Module Demo
This is demo and documentation of SpinWheel dropin modules. Where you can render dropin modules with build file. You can pass configuration to load our dropin modules.

## Drop In Modules
---
 - White label plug & play UI modules for proven end user flows
 - Option to integrate individual dropin modules anywhere in the app
 - Customized to match partner’s user experience and visual design
 - Completely responsive and designed to work on desktop browser, mobile web and as web-view within iOS or Android mobile app
 - Simple integration with a single web url
 - Seamless integration with sso handshake, oAuth or custom webhook to verify partner user’s identity

### Modules List

##### Currently, we provide 10 dropin modules:
 - **Connect Drop in Module** - a whitelabel, secure, elegant authentication flow for users to link their loans.
 - **Loan Pal Drop in Module** - an interactive dashboard and goal oriented experience
 - **Precision Pay Drop in Module** - a white label payment flow
 - **Loan List Drop in Module** - a white label loans view
 - **Transaction History Drop in Module** - a white label transaction view
 - **Student Loan Refi Drop in Module** - a white label Student Loan Refi view
 - **Auto Refi Drop in Module** - a white label Auto Refi view
 - **Personal Refi Drop in Module** - a white label Personal Refi view
 - **Auto Debit Drop in Module** - a white label auto payment flow
 - **Debt Connect Drop in Module** - a white label debt connection flow
 - **Identity Connect Drop in Module** - a white label flow to validate a users identity


## Implementation
---
Include the SpinWheel dim(dropin module) initialize script on your page. It should always be loaded directly from https://cdn.spinwheel.io, rather than included in a bundle or hosted yourself. When script is loaded you can invoke create method with configuration object.
```javascript
<script src="https://cdn.spinwheel.io/dropin/v1/dim-initialize.js"></script>
```

### Create
Spinwheel.create accepts one argument, a configuration Object, and returns an Object with two functions, open and exit. Calling **open** will render appropriate dropin module and calling **exit** will close the dropin module.
Configuration object takes a unique element id where dropin module will be rendered and a valid token given by spinwheel api.

```javascript
const handler = Spinwheel.create({
  containerId: 'DROPIN_CONTAINER_ELEMENT_ID',
  onSuccess: (metadata) => {},
  onLoad: () => {},
  onExit: (metadata) => {},
  onEvent: (metadata) => {},
  onError: (metadata) => {},
  onResize: (metadata) => {},
  dropinConfig: {
    'module': 'loan-servicers-login',
    'token': '{token}'
  }
});


handler.open() // to render dropin module
handler.exit() // to close dropin module
```

## Callback Methods
---
#####  onSuccess
It is called at the end point of dropin experience when purpose of dropin is fullfilled. Like, in connect dropin module when user has authenticated his loan account. On callback method we provide necessary data in method parameter and close the dropin module.
##### onLoad
It is a callback which is called when dropin module has finished loading. You can pass this method on configuration object.
#####  onExit
onExit callback is called when you close the dropin module with close navigation button, which is provided at the top right of page. Exit/close button is an optional feature which you can enable by passing 'showExitNavigation' property to [dropinConfig](#markdown-header-create) object.
##### onEvent
onEvent callback is called to trigger importart event during lifetime of dropin experience. In this callback we provide useful information in method parameter so that you can be informed of every crucial event that matters most to you.
##### onError
onError callback is called when dropin module throws an error. You can leverage this callback to do necessary handling on your side.
##### onResize
onResize callback is called when size of dropin changes due to page resize or other events. In this callback we provide height and width of dropin module document so you can customise the experince for different devices. Currently, our dropin modules switches to mobile experience on 768px screen width.
##### onSettled
onSettled callback is called when the dropin module has finished loading and completed with all api requests and has settled for the user to start operating.
##### pageChange
onPageChange callback is called when the control goes from one page to another in the dropin.

| Drop in Module/Events |  |  |  | | | | |
| ------ | ------ | ------ | ------ |  ------ |  ------ |  ------ | ------ |
| loan-servicers-login | [onSuccess](#markdown-header-onsuccess_1) | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_2) | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | |
| precision-pay | [onSuccess](#markdown-header-onsuccess_2) | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_3) | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| loan-pal |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_1) | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| loan-list |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_1) | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| transaction-history |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_1) | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| student-loan-refi | | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_4) | [onError](#markdown-header-onerror_2) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| auto-refi |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_5) | [onError](#markdown-header-onerror_3) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| personal-refi |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | [onEvent](#markdown-header-onevent_6) | [onError](#markdown-header-onerror_4) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| auto-debit |  | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | |
| debt-connect | [onSuccess](#markdown-header-onsuccess_1) | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) | [onSettled](#markdown-header-onsettled_1) |
| identity-connect | [onSuccess](#markdown-header-onsuccess_1) | [onLoad](#markdown-header-onload_1) | [onExit](#markdown-header-onexit_1) | | [onError](#markdown-header-onerror_1) | [onResize](#markdown-header-onresize_1) |  |

### Common Methods
Callback methods applicable for all dropin modules
#### onLoad
　
```javascript
onLoad(metadata){
	// your code
}
```
MetaData Schema
```sh
{
    'eventName': 'DROPIN_LOAD',
    'message': `${dropin} dropin module loaded.`
}
```
#### onExit
　
```javascript
onExit(metadata){
	// your code
}
```
To use onExit callback, first you need to enable exit navigation on dropin. Provide {'showExitNavigation' : 1 } key/value to [dropinConfig](#markdown-header-create) object to use this feature.

MetaData Schema
```sh
{
    'eventName': 'DROPIN_EXIT',
    'message': 'Drop in Module Closed.'
}
```
#### onError
　
```javascript
onError(metadata){
	// your code
}
```
##### Some of the error events when onError method will be called.
 - DROPIN_CONTAINER_NOT_PASSED - when an element id is not passed
 - DROPIN_CONTAINER_UNAVAIL - dropin container element doesn't exist
 - DROPIN_TOKEN_UNAVAIL - token isn't passed
 - DROPIN_LOAD_FAIL - dropin module fails to load, due to invalid token or other config related values
 - PAYMENT_FAIL - payment scheduling api fails
 - SUBSCRIPTION_FAIL- subscription save api fails
 - API_TIMEOUT - api call exceeds 120 seconds
 - OPEN_METHOD_DUPLICATE_CALL - duplicate call to handler.open method
 - EXIT_CALLBACK_UNAVAIL - exit callback function is not provided when onExit event is called by dropin
 - SUCCESS_CALLBACK_UNAVAIL - success callback function is not provided when onSuccess is called by the dropin
 - DIM_UNAVAILABLE_OUTAGE - dropin module server is not available
 - INVALID_MODULE_NAME - invalid module name is passed
 - DIM_ERROR - some unexpected error occured in dropin
 - INVALID_TOKEN - invalid token is passed
 - EXPIRED_TOKEN - token is expired upon request sent or expiry time has elapsed for that token

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        // details related to error
    }
}
```
#### onResize
　
```javascript
onResize(metadata){
	// your code
}
```
MetaData Schema
```sh
{
    'eventName': 'DROPIN_RESIZE',
    'message': 'Dropin window size changed.',
    'metadata': {
        'height': 'height of dropin document', // in pixels like 786
        'width':  'width of dropin document' // in pixels like 500
    }
}
```
#### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
##### common event when onEvent method will be called.
 - AUTH_EXPIRE - method is called when user credentials is expired/updated. This method would be fired by all dropin modules except connect dropin module. On metadata, we also dispense **noValidLoan** and **reauthenticateAccounts** attributes. Where **noValidLoan** informs whether a user has any valid loan accounts or not and **reauthenticateAccounts** gives you an array of loan accounts that needs re-authentication.

When consumer subscribe for this event and when this event is fired. We send this callback to consumer and unmount the dropin module. So that partner can present their own experience for re-authentication of accounts. If this event is not subscribed by consumer, we show our default screen where user would go through re-auth flow to re-authenticate his/her account.

MetaData Schema
```sh
{
    'eventName': 'AUTH_EXPIRE',
    'message': 'Credentials Expired/Updated.',
    'metadata': {
        'extUserId': '{extUserId}',
        'shouldReconnect':  true,
        'noValidLoan': false,
        'reauthenticateAccounts': "<array of accounts needing re-auth>"
    }
}
```

 - CONNECT_LOAN_TO_CONTINUE - method is called when a user doesn't have any loans. This method would be fired by all dropin modules except connect dropin module. On metadata, we also dispense **userInfo** object.

MetaData Schema
```sh
{
    'eventName': 'CONNECT_LOAN_TO_CONTINUE',
    'message': 'Connect loan to use this Dropin.',
    'metadata': {
        'extUserId': '{extUserId}',
        'userInfo': '<userInfo>'
        "dimExitInitiated": true
    }
}
```
#### onSettled
　
```javascript
onSettled(metadata){
	// your code
}
```
MetaData Schema
```sh
{
    'eventName': 'DIM_SETTLED',
    'message': `${dropin} Dropin Load complete`
}
```

### Module Specific Methods
#### Connect Drop in Module

##### onSuccess
　
```javascript
onSuccess(metadata){
	// your code
}
```
This method is called when user successfully authenticate his/her loan account. We pass token in callback method metadata and close the dropin.

MetaData Schema
```sh
{
    'eventName': 'AUTH_SUCCESS',
    'message': 'User authentication is successful',
    'metadata': {
        'token': '{jwt}',
    }
}
```
You can get userId and extUserId by parsing the token with help of a library like ‘jwt-decode’.
```javascript
import jwtDecode from "jwt-decode";
const decodedValues = jwtDecode(token);
```

A decoded token can have 2 types of values.

Type I - This is a new loan servicer account, which results in a new user on the spinwheel platform

```sh
{
    'userId': '{userId}',
    'extUserId': '{extUserId}',
}
```

Type II  - Multiple users have connected to this loan servicer account

```sh
{
    'matchedUsers': [
        {
            'userId': '{userId}',
            'extUserId': '{extUserId}',
        },
        {
            'userId': '{userId}',
            'extUserId': '{extUserId}',
        }
    ],

}
```

##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following event methods are called on connect dropin module.
 - INVALID_LOGIN_ATTEMPT - user attempt to authenicate with invalid credentials
 - USER_LOGGED_OUT - user logged out for security reasons
 - USER_ACCOUNT_LOCKED- user account is locked
 - LOGIN_REQUEST_TIMEOUT - authentication api timed out

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}'
}
```

#### Precision Pay Drop in Module
##### onSuccess
　
```javascript
onSuccess(metadata){
	// your code
}
```
User create a payment and move to payment confirmation screen. onSuccess method is called when user wish to close dropin and clicks on **Done** button. We pass the necessary data in event callback and close the dropin module.

MetaData Schema
```sh
{
    'eventName': 'PAYMENT_DROPIN_CLOSE',
    'message': 'Payment Dropin Closed',
    'metadata': {
        'daysSaved': '{daysSaved}',
        'savingsAmount': '{savingsAmount}'
    }
}
```
##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following event methods are called on precision pay dropin module.
 - PAYMENT_SCH - payment is scheduled

MetaData Schema
```sh
{
    'eventName': 'PAYMENT_SCH',
    'message': 'Payment Processed',
    'metadata': {
        'paymentAmount': '{paymentAmount}'
    }
}
```
 - SUBSCRIPTION_SAVE - subsciption is saved/updated

MetaData Schema
```sh
{
    'eventName': 'SUBSCRIPTION_SAVE',
    'message': 'Subscription Saved',
    'metadata': {
        'paymentAmount': '{paymentAmount}',
        'mode': 'oneTime | recurring | roundUp'
    }
}
```

#### Student Loan Refi Dropin Module
##### onError
　
```javascript
onError(metadata){
	// your code
}
```
###### Following error methods are called on Student Loan Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```
##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following event methods are called on Student Loan Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```
#

#### Auto Refi Dropin Module
##### onError
　
```javascript
onError(metadata){
	// your code
}
```
###### Following error methods are called on Auto Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```
##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following event methods are called on Auto Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out
 - APPLICATION_OFFER - user has selected an offer for refinance

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```

#
#### Personal Refi Dropin Module
##### onError
　
```javascript
onError(metadata){
	// your code
}
```
###### Following error methods are called on Personal Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```
##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following event methods are called on Personal Refi dropin module.
 - APPLICATION_CREATE - user's refinance application is created
 - APPLICATION_UPDATE - user's refinance application is updated
 - APPLICATION_APPLY- user has applied for refinance
 - APPLICATION_RESULTS - user's refinance application's results are out
 - APPLICATION_OFFER - user has selected an offer for refinance

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        'userId': '{userId}',
        'extUserId': '{extUserId}',
        'loanAmount': '{loanAmount}',
        'status': 'NEW' | 'DRAFT' | 'APPLIED',
        'error': '{error}'
    },
}
```
#

#### Identity Connect Dropin Module

By default, identity connect will render blank inputs. A phone number, birthday month, or birthday day may to be passed directly to the module to save the user from having to enter the information themselves. To do so, pass the information in the dropinConfig object as illustrated below:

```javascript
  dropinConfig: {
    module: 'identity-connect',
    token: token,
    phoneNumber: '2066604710',
    month: 12,
    day: 2
  },
```

All three parameters may be passed as either a string or a number, but they will be parsed and validated when the module loads. In the case that an invalid parameter has been passed (input - month: 2, day: 31), the invalid parameter will be treated as if it were not passed at all (output - month: 2, day: undefined).

##### onError
　
```javascript
onError(metadata){
	// your code
}
```
###### Following onError methods are called on Identity Connect dropin module.
 - API_TIMEOUT - More than 90 seconds elapsed, connection attempt timed out.
 - DIM_ERROR - Identity connect encountered an internal error.

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
}
```
##### onEvent
　
```javascript
onEvent(metadata){
	// your code
}
```
###### Following onEvent methods are called on Identity Connect dropin module.
 - USER_SUBMISSION - User submitted information connection process initiated.
 - LINK_CLICKED - Link sent to user's device was clicked.
 - IDENTITY_CONNECTED - User successfully connected their identity.
 - IDENTITY_NOT_CONNECTED - Attempt to connect user's identity has failed.

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
}
```

#
### Dim error callback events
 - DROPIN_CONTAINER_UNAVAIL - dropin container element doesn't exist
 - DROPIN_TOKEN_UNAVAIL - token isn't passed
 - DIM_UNAVAILABLE_OUTAGE - dropin module server is not available
 - DIM_ERROR - some error occured in dropin
 - INVALID_MODULE_NAME - invalid module name is passed
 - INVALID_TOKEN - invalid token is passed
 - EXPIRED_TOKEN - token is expired upon request sent or expiry time has elapsed for that token

MetaData Schema
```sh
{
    'eventName': '{eventName}',
    'message': '{message}',
    'metadata': {
        module: <module_name>
    }
}
```
#
### Token Expired Handling
When a token expires within our Drop-in Modules (DIM), We send an onError event and present a screen to inform this:

MetaData Schema
```sh
{
    "eventName": "EXPIRED_TOKEN",
    "message": "Looks like token is expired",
    "metadata": {
        "module": "loan-servicers-login"
    }
}
```

Here you have the option to skip the session expired screen and handle the experience at your end. To remove the session expired screen you need to pass **{showTokenExpiredPage: 0}** :

MetaData Schema
```sh
{
    module: 'loan-servicers-login',
    token,
    showTokenExpiredPage: 0
}

```

Moreover, you can also customize the given screen’s all details like title, subtitle, button text:

MetaData Schema
```sh
  {
    module: 'loan-servicers-login',
    token,
    tokenExpiredPageTitle: 'Your session has expired.',
    tokenExpiredPageSubTitle: 'You’ve timed out due to inactivity. You may close and try again.',
    tokenExpiredPageExitBtnLabel: 'Close'
}

```

On Click of close button on session expired screen, we send an onExit callback and close the DIM. So, if needed, you can relaunch the DIM with a fresh token.
MetaData Schema
```sh
{
    "eventName": "DROPIN_EXIT",
    "message": "Drop in Module Closed."
}

```

